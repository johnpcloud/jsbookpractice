/*
===============================================
	5.3.2 Functions
===============================================
*/

/*
// A recursive function
function factorial(n) { 
	if (n <= 1) return 1;
	return n * factorial(n - 1);
}
 var r = factorial(10);
 console.log(r);
*/




/*
===============================================
	5.5 Loops
===============================================
*/ 




/*
// For loop
for (var i=0; i<10; i++){
    console.log("The Value of i Is " + i);
}
*/


/*
// While Loop
var i=8;
while (i<10){
	console.log("I is less than 10");
	i++;
}
*/


/*
// Do-While Loop
var i=7;
do{
    console.log("The value of i is " + i);
    i++;
}
while(i>7 && i<10);
*/


/*
// forEach Loop
var arr=[10, 20, "hi", ,{}];
arr.forEach(function(item, index){
    console.log(' arr['+index+'] is '+ item);
});
*/


/*
// for...in loop
var obj = {a: 1, b: 2, c: 3};    
for (var prop in obj) {
    console.log('obj.'+prop+'='+obj[prop]);
};
*/

/*
// for...of Loop
var str= 'paul';
for (var value of str) {
console.log(value);
}
*/



/*
===============================================
	5.6.5 Throw
===============================================
*/


function getRectArea(width, height) {
  if (isNaN(width) || isNaN(height)) {
    throw "Parameter is not a number!";
  }
  else{
  	return width * height;
  }
}

try {
  console.log(getRectArea(3, 'as'));
}
catch(e) {
  console.log(e);
}


